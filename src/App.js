import React from 'react';
import { Pokemons } from './features/pokemon/Pokemons';
import './App.css';

function App() {
  return (
    <div className="App">
      <Pokemons />
    </div>
  );
}

export default App;
