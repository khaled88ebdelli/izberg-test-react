import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
  fetchPokemonsAsync,
  fetchPokemonByNameAsync,
  removeFromFavorits,
  selectPokemonState,
  addToFavorits
} from './pokemonSlice';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
  faPlusSquare,
  faCheckCircle,
  faTrash,
  faStar,
  faArrowRight,
  faArrowLeft,
  faEye
} from '@fortawesome/free-solid-svg-icons'
import Modal from '../../components/Modal';
import style from './Pokemon.module.css';

export function Pokemons() {
  const [isOpen, setIsOpen] = useState(false)
  const [isDetailsOpen, setIsDetailsOpen] = useState(false)
  const { list, loading, favorits, selected, next, previous } = useSelector(selectPokemonState);
  const dispatch = useDispatch();
  const closeModal = () => setIsOpen(false)
  const closeDetailsModal = () => setIsDetailsOpen(false)

  useEffect(() => {
    dispatch(fetchPokemonsAsync())
  }, [dispatch])


  const HandleRemoveFromFavorits = (pokemon) => dispatch(removeFromFavorits(pokemon))
  const HandleAddToFavorits = (pokemon) => dispatch(addToFavorits(pokemon))
  const HandleNextPage = () => dispatch(fetchPokemonsAsync(next))
  const HandlePreviousPage = () => dispatch(fetchPokemonsAsync(previous))
  const HandleSelectUser = (url) => {
    dispatch(fetchPokemonByNameAsync(url))
    setIsDetailsOpen(true)
  }

  return (
    <div>
      <h3 className={style.favoritsButon} onClick={() => setIsOpen(true)}>
        Show Favorits <FontAwesomeIcon style={{ marginLeft: '5px' }} icon={faStar} />
      </h3>
      <div className={style.pagination}>
        <button onClick={() => HandlePreviousPage()} disabled={!previous}>
          <FontAwesomeIcon icon={faArrowLeft} />
        </button>
        <button disabled={!next} onClick={() => HandleNextPage()}>
          <FontAwesomeIcon icon={faArrowRight} />
        </button>
      </div>
      <div className={style.gridContainer}>
        {list.length > 0 && !loading &&
          list.map((pokemon, index) => (
            <div className={style.card} key={index}>
              <div className={style.content}>
                <h5>{pokemon.name}</h5>
                <FontAwesomeIcon
                  style={{ color: pokemon.fav ? 'green' : '' }}
                  className={style.addFavIcom}
                  onClick={() => HandleAddToFavorits(pokemon)}
                  icon={!!pokemon.fav ? faCheckCircle : faPlusSquare} />
                <FontAwesomeIcon
                  className={style.showIcom}
                  onClick={() => HandleSelectUser(pokemon.url)}
                  icon={faEye} />
              </div>
            </div>
          ))
        }
        {loading && <h1>Loading...</h1>}
        <Modal isOpen={isOpen} closeModal={closeModal} title="Favorits">
          <div className={style.gridContainer}>
            {favorits.map((f, index) => {
              return <div className={style.card} key={index}>
                <div className={style.content}>
                  <h3 style={{ textAlign: 'center' }}>{f.name}</h3>
                  <FontAwesomeIcon
                    style={{ color: 'red' }}
                    onClick={() => HandleRemoveFromFavorits(f)}
                    icon={faTrash} />
                </div>
              </div>
            })}
          </div>
        </Modal>
        {selected && <Modal isOpen={isDetailsOpen} closeModal={closeDetailsModal} title={selected.name}>
          <div className={style.gridContainer}>
            <div className={style.card}>
              <div className={style.bgImg}>
                <img src={selected.sprites.front_default} alt={selected.name} />
                <img src={selected.sprites.back_shiny} alt={selected.name} />
                <img src={selected.sprites.back_default} alt={selected.name} />
              </div>
              <div className={style.content}>
                <h3 style={{ textAlign: 'center' }}>{selected.name}</h3>
                <h4> Abilities :
                  {selected.abilities.map((a, i) => <p key={i}> ({i}) {a.ability.name}</p>)}
                </h4>
                <h4> base_experience : {selected.base_experience}</h4>
              </div>
            </div>
          </div>
        </Modal>}
      </div>
    </div>
  );
}
