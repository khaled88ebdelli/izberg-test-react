import { createSlice } from '@reduxjs/toolkit';


const apiUrl = 'https://pokeapi.co/api/v2/pokemon'
export const slice = createSlice({
  name: 'pokemon',
  initialState: {
    list: [],
    loading: false,
    selected: null,
    favorits: localStorage.getItem('favorits') ? JSON.parse(localStorage.getItem('favorits')) : [],
    error: null,
    next: "",
    previous: null,
  },
  reducers: {
    fetchPokemon: state => {
      state.loading = true;
    },
    fetchPokemonSuccess: (state, action) => {
      state.next = action.payload.next
      state.previous = action.payload.previous
      state.list = action.payload.results.map(p => {
        const exist = state.favorits.find(f => f.name === p.name)
        if (exist) {
          return { ...p, fav: true }
        }
        return p
      })
      console.log('previous', state.previous);
      console.log('next', state.next);
      state.loading = false
    },
    fetchPokemonByNameSuccess: (state, action) => {
      state.selected = action.payload
      state.loading = false
    },
    fetchPokemonFailed: (state, action) => {
      state.loading = false
      state.error = action.payload.message
    },
    addToFavorits: (state, action) => {
      const exist = state.favorits.find(f => f.name === action.payload.name)
      if (!exist) {
        state.favorits.push(action.payload)
        state.list = state.list.map(p => {
          if (p.name === action.payload.name) {
            return { ...p, fav: true }
          }
          return p
        })
        localStorage.setItem('favorits', JSON.stringify(state.favorits))
      }

    },
    removeFromFavorits: (state, action) => {
      state.favorits = state.favorits.filter(f => f.name !== action.payload.name)
      state.list = state.list.map(p => {
        if (p.name === action.payload.name) {
          return { ...p, fav: false }
        }
        return p
      })
      localStorage.setItem('favorits', JSON.stringify(state.favorits))

    },
  },
});

export const {
  fetchPokemon,
  fetchPokemonSuccess,
  fetchPokemonFailed,
  fetchPokemonByNameSuccess,
  addToFavorits,
  removeFromFavorits
} = slice.actions;

export const fetchPokemonsAsync = (url = apiUrl) => dispatch => {
  dispatch(fetchPokemon())
  return fetch(url)
    .then((response) => {
      if (!response.ok) throw new Error('Error - 404 Not Found')
      return response.json()
    })
    .then(res => dispatch(fetchPokemonSuccess(res)))
    .catch(err => fetchPokemonFailed(err))
};

export const fetchPokemonByNameAsync = (url) => dispatch => {
  dispatch(fetchPokemon())
  fetch(url)
    .then((response) => {
      if (!response.ok) throw new Error('Error - 404 Not Found')
      return response.json()
    })
    .then(res => dispatch(fetchPokemonByNameSuccess(res)))
    .catch(err => fetchPokemonFailed(err))
};


export const selectPokemonState = state => state.pokemon;

export default slice.reducer;
