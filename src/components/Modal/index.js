import React from 'react'
import Modal from 'react-modal';
import { faWindowClose } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import './index.css'
Modal.setAppElement('#root');

const Index = ({ isOpen, closeModal, title, children }) => {
    return (
        <div>
            <Modal
                isOpen={isOpen}
                onRequestClose={closeModal}
                className="Modal"
                overlayClassName="Overlay"
                contentLabel={title}
            >
                <FontAwesomeIcon
                className="display-topright"
                onClick={() => closeModal()}
                icon={faWindowClose} />
                {children}
            </Modal>
        </div>
    )
}

export default Index
